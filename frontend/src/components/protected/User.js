import React from 'react';
import { getAuth } from "firebase/auth";
import { Navigate } from 'react-router-dom';

const User = () => {
    const auth = getAuth();
    const user = auth.currentUser;

    var photoURL = '';
    var userName = '';
    if (user != null) {
        user.providerData.forEach(providerData => {
             photoURL = providerData.photoURL ? providerData.photoURL : photoURL;
             userName = providerData.displayName ? providerData.providerId + ':' + providerData.displayName : userName;
        })
    }

    return (
        <div>
            {user ? (
                <>
                    <p>{userName}</p>
                    <img alt={userName} src={user.providerData[0].photoURL} />
                </>
            ) : (
                <Navigate to="/" />
            )}

        </div>
    );
};

export default User;