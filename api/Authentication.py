from flask import jsonify
from flask_restful import Api, Resource, reqparse
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity, get_jwt

from firebase_admin import credentials, firestore, initialize_app

from passlib.hash import pbkdf2_sha256 as sha256

from datetime import datetime, timedelta, timezone

class Registration(Resource):
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('username', help = 'This field cannot be blank', required=True)
        parser.add_argument('email', help = 'This field cannot be blank', required=True)
        parser.add_argument('password', help = 'This field cannot be blank', required=True)

        db = firestore.client()
        data = parser.parse_args()
        # Check if user already exists
        users = db.collection('users').where('username', '==', data["username"]).get()

        if len(users) == 0:
            new_user_ref = db.collection('users').document()
            try:
                new_user_ref.set({
                    'username': data['username'],
                    'email': data['email'],
                    'password': sha256.hash(data['password']),
                    'admin': False
                })
                additional_claims = { "admin": False }
                access_token = create_access_token(identity = new_user_ref.id, additional_claims=additional_claims)
                refresh_token = create_refresh_token(identity = new_user_ref.id, additional_claims=additional_claims)
                return {
                    'message': f'User { data["username"] } was created.',
                    'access_token': access_token,
                    'refresh_token': refresh_token
                }
            except Exception as e:
                return { 'message': 'Something went wrong' }, 500
        else:
            return { 'message': 'Username already taken'}

class Login(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('username', help = 'This field cannot be blank', required=True)
        parser.add_argument('password', help = 'This field cannot be blank', required=True)

        db = firestore.client()
        data = parser.parse_args()
        # Since usernames are unique, this should always yield exactly 1 user
        user = db.collection('users').where('username', '==', data["username"]).get()[0]
        if user.exists:
            userData = user.to_dict()
            #if data["password"] == userData["password"]:
            if sha256.verify(data['password'], userData['password']):
                additional_claims = { "admin": userData['admin'] }
                access_token = create_access_token(identity = user.id, additional_claims=additional_claims)
                refresh_token = create_refresh_token(identity = user.id, additional_claims=additional_claims)
                return {
                    'message': f'Logged in as { userData["username"] }',
                    'access_token': access_token,
                    'refresh_token': refresh_token
                }
            else:
                return {
                    'message': f"Wrong credentials."
                }
        else:
            return {
                'message': f"Wrong credentials."
            }

class Refresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        current_user = get_jwt_identity()
        print(current_user)
        access_token = create_access_token(identity = current_user)
        return { 'access_token': access_token }

# TODO: Move this to Redis or Realtime Database
class LogoutAccess(Resource):
    @jwt_required()
    def post(self):
        jti = get_jwt()['jti']
        try:
            db = firestore.client()
            # Check if user already exists
            users = db.collection('revokedTokens').document(jti).set({
                'timestamp': firestore.SERVER_TIMESTAMP
            })
            return {'message': 'Access Token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500
        
# TODO: Move this to Redis or Realtime Database
class LogoutRefresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        jti = get_jwt()['jti']
        try:
            db = firestore.client()
            # Check if user already exists
            users = db.collection('revokedTokens').document(jti).set({
                'timestamp': firestore.SERVER_TIMESTAMP
            })
            return {'message': 'Access Token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500
