from flask_restful import Api, Resource, reqparse
from flask_jwt_extended import jwt_required, get_jwt_identity

# Firestore
from firebase_admin import firestore

class Profile(Resource):
    @jwt_required()
    def get(self):
        db = firestore.client()
        try:
            current_user = get_jwt_identity()
            user = db.collection('users').document(current_user).get()
            userData = user.to_dict()
            return {
                'id': user.id,
                'username': userData['username'],
                'email': userData['email'],
                'admin': userData['admin']
            }, 200
        except Exception as e:
            return {'msg': f"An Error Occured: {e}"}, 500