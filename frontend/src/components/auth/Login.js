import React, { useState } from 'react';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import { getAnalytics, logEvent } from 'firebase/analytics';

const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const auth = getAuth();
    const analytics = getAnalytics();
    const handleLogin = () => {
        signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            const user = userCredential.user;
            logEvent(analytics, 'login', {
                method: user.providerId
              });
            console.log("Signed in user: ", user);
        })
        .catch((error) => {
            console.log("An error occured: ", error.code, error.message);
        })
    };
    return (
        <div>
            <h1>Login</h1>
            Email:
            <br />
            <input
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            />
            <br />
            Password:
            <br/>
            <input
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
            />
            <br />
            <button onClick={handleLogin}>Log In</button>
        </div>
    );
};

export default Login;