import React, { useState } from "react";
import { getAuth, sendPasswordResetEmail } from "firebase/auth";

const Reset = () => {
  const [email, setEmail] = useState("");
  const auth = getAuth();
  const handleReset = () => {
    sendPasswordResetEmail(auth, email)
    .then(() => {
      console.log("success");
    })
    .catch((error) => {
      console.log("An error has occured: ", error.code, error.message);
    })
  };
  return (
    <div>
      <h1>Reset password</h1>
      Email:
      <br />
      <input
        type="text"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <br />
      <button onClick={handleReset}>Reset password</button>
    </div>
  );
};

export default Reset;