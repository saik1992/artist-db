import React from 'react';
import { useSelector } from "react-redux";
import { Navigate } from 'react-router-dom';

import { useGetPokemonByNameQuery } from '../../services/pokemon';

const Secret = () => {
    const user = useSelector((state) => state.auth.value);

    // Using a query hook automatically fetches data and returns query values
    const { data, error, isLoading } = useGetPokemonByNameQuery('bulbasaur')
    // Individual hooks are also accessible under the generated endpoints:
    // const { data, error, isLoading } = pokemonApi.endpoints.getPokemonByName.useQuery('bulbasaur')

    return (
        <div>
            {user ? (
                <>
                    <h1>Secret page</h1><div>
                        {error ? (
                            <>Oh no, there was an error</>
                        ) : isLoading ? (
                            <>Loading...</>
                        ) : data ? (
                            <>
                                <h3>{data.species.name}</h3>
                                <img src={data.sprites.front_shiny} alt={data.species.name} />
                            </>
                        ) : null}
                    </div>
                </>
            ) : (
                <Navigate to="/" />
            )}

        </div>
    );
};

export default Secret;