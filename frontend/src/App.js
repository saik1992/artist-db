import * as React from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';

import { AppBar, Box, IconButton, Toolbar, Typography, Button, Container, Menu, MenuItem, Tooltip, Avatar } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';

import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Reset from './components/auth/Reset';
import Home from './components/Home';
import Secret from './components/protected/Secret';
import User from './components/protected/User';
import Logout from './components/auth/Logout';

import { initializeApp } from 'firebase/app';
import { firebaseConfig } from './configs/firebaseConfig';
import { getAuth, onAuthStateChanged } from "firebase/auth";

import { useSelector, useDispatch } from "react-redux";
import { saveUser } from "./redux/slices/authSlice";

const pages = ['Home', 'Menu1', 'Menu2'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const App = () => {
  // Firebase setup
  initializeApp(firebaseConfig);
  const auth = getAuth();
  // Get user name and photo
  var photoURL = '';
  var userName = '';
  if (auth.currentUser != null) {
    auth.currentUser.providerData.forEach(providerData => {
      photoURL = providerData.photoURL ? providerData.photoURL : photoURL;
      userName = providerData.displayName ? providerData.displayName : userName;
    })
  };
  // User state setup
  const user = useSelector((state) => state.auth.value);
  const dispatch = useDispatch();
  React.useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        handleCloseUserMenu();
        dispatch(saveUser(user.refreshToken));
      } else {
        dispatch(saveUser(undefined));
      }
    });
  }, [auth, dispatch]);
  // AppBar state and state handling
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <BrowserRouter>
      <AppBar position="static">
        <Container maxWidth="x1">
          <Toolbar disableGutters>
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size="large"
                aria-label="menu of the webpage"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left'
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                xc={{
                  display: { xs: 'block', md: 'none' },
                }}
              >
                {pages.map((page) => (
                  <MenuItem key={page} component={Link} to={"/" + page.toLowerCase()} onClick={handleCloseNavMenu}>
                    <Typography textAlign="center">{page}</Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
            <Typography variant="h6" noWrap component="div" sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}>
              Artist-DB
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              {pages.map((page) => (
                <Button
                  key={page}
                  onClick={handleCloseNavMenu}
                  sx={{ my: 2, color: 'white', display: 'block' }}
                  component={Link} to={"/" + page.toLowerCase()}>
                  {page}
                </Button>
              ))}
            </Box>
            {user ? (
            <Box sx={{ flexGrow: 0 }}>
              <Tooltip title="Open settings">
                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                  <Avatar alt={userName} src={photoURL} />
                </IconButton>
              </Tooltip>
              <Menu
                sx={{ mt: '45px' }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'origin',
                  horizontal: 'right',
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu} >
                  {settings.map((setting) => (
                    <MenuItem key={setting} component={Link} to={"/" + setting.toLowerCase()} onClick={handleCloseUserMenu}>
                      <Typography textAlign="center">{setting}</Typography>
                    </MenuItem>
                  ))}
              </Menu>
            </Box>
            ) : (
              <Button color="inherit" component={Link} to="/login">Login</Button>
            )}

          </Toolbar>
        </Container>
      </AppBar>
      <Routes>
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/profile" element={<User />} />
        <Route path="/account" element={<User />} />
        <Route path="/reset" element={<Reset />} />
        <Route path="/protected" element={<Secret />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/" element={<Home />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

/* const temp = (
  
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        {user ? (
          <>
            <li>
              <Link to="/user">Profile</Link>
            </li>
            <li>
              <Link to="/protected">Protected page</Link>
            </li>
            <li>
              <Link to="/reset">Reset password</Link>
            </li>
            <li>
              <Link
                to="#"
                onClick={() => {
                  signOut(auth)
                    .then(() => {
                      console.log("user signed out");
                    })
                    .catch((error) => {
                      console.log("error", error);
                    });
                }}>Logout</Link>
            </li>
          </>
        ) : (
          <>
            <li>
              <Link to="/login">Login</Link>
            </li>
            <li>
              <Link to="/register">Register</Link>
            </li>
          </>
        )}
      </ul>
    </nav>
    <Routes>
      <Route path="/register" element={<Register />} />
      <Route path="/login" element={<Login />} />
      <Route path="/user" element={<User />} />
      <Route path="/reset" element={<Reset />} />
      <Route path="/protected" element={<Secret />} />
      <Route path="/" element={<Home />} />
    </Routes>
  </BrowserRouter >
); */