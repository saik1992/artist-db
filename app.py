# flask run --host=0.0.0.0 to run

# Flask Framework and Extensions
from flask import Flask, send_from_directory
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_cors import CORS # comment this on deployment

# Firestore
from firebase_admin import credentials, firestore, initialize_app

# Python-native modules
from datetime import timedelta

# Api Endpoints
from api.Authentication import Registration, Login, LogoutAccess, LogoutRefresh, Refresh
from api.Profile import Profile
from api.SecretResource import SecretResource
from api.PublicResource import PublicResource

# Initialize Flask app
app = Flask(__name__, static_url_path='', static_folder='frontend/build')
# Firestore
cred = credentials.Certificate('db/key.json')
default_app = initialize_app(cred)
# JWT
app.config["JWT_SECRET_KEY"] = "please-remember-to-change-me"
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=1)
app.config["JWT_BLACKLIST_ENABLED"] = True
app.config["JWT_BLACKLIST_TOKEN_CHECKS"] = ['access', 'refresh']
jwt = JWTManager(app)
# flask_restful
CORS(app) # comment this on deployment
api = Api(app)

@app.route("/", defaults={'path':''})
def serve(path):
    return send_from_directory(app.static_folder,'index.html')

@jwt.token_in_blocklist_loader
def check_if_token_in_blacklist(token_header, encoded_token):
    jti = encoded_token['jti']
    db = firestore.client()
    revokedToken = db.collection('revokedTokens').document(jti).get()
    return revokedToken.exists

# Auth Endpoints
api.add_resource(Registration, '/api/auth/register')
api.add_resource(Login, '/api/auth/login')
api.add_resource(LogoutRefresh, '/api/auth/logout/refresh')
api.add_resource(LogoutAccess, '/api/auth/logout/access')
api.add_resource(Refresh, '/api/auth/refresh')
# Api Endpoints
api.add_resource(Profile, '/api/profile')
api.add_resource(SecretResource, '/api/secret')
api.add_resource(PublicResource, '/api/public')