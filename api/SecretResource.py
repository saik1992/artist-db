from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt

class SecretResource(Resource):
    @jwt_required()
    def get(self):
        claims = get_jwt()
        if claims['admin']:
            return {
                'answer': 42
            }
        else:
            return {}, 401