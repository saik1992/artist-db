import { useNavigate } from 'react-router-dom';
import { getAuth, signOut } from "firebase/auth";

const Logout = () => {
    const auth = getAuth();
    let navigate = useNavigate();
    signOut(auth)
        .then(() => {
            navigate("/");
        })
        .catch((error) => {
            console.log("error", error);
        });
    return (
        <></>
    );
};

export default Logout;